# <ev-budget-planner\>

The `ev-budget-planner` widget helps to define expenses in retirement. This widget utilizes BudgetPlannerRetirement API within EValue's API Portal. This widget consists of two parts (calls).
The first call is optional, returning the budget items and typical high/average/low net annual spend for each one.
The second call enters the value selected for each of the budget items, calculates the net annual amount needed to meet these, and from this the gross amount that would need to be withdrawn based on current income tax bands and rates.

## Browser Support
This component has been tested with the following browsers

* **Google Chorme**
* **Firefox**
* **Microsoft Edge**
* **Safari**

## Installation
---
Although the ev-budget-planner is a bower package, it is not registered in the bower registry. To install it you will need to tell bower CLI where to look for the package. So intead of running

```sh
$ bower install –-save ev-budget-planner-public
```
you will need to run:

```sh
$ bower install --save https://bitbucket.org/evuk/ev-budget-planner-public
```
Alternatively, you can create a bower.json by hand

```json
{
    "name": "your-application",
    "dependencies": {
		"ev-budget-planner-public": "https://bitbucket.org/evuk/ev-budget-planner-public.git#v1.0.0"
    }
}
```
Then run ```bower install``` in the directory where the bower.json resides.

## Including component in HTML Example
---

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes">
    <title>Demo</title>
    <link rel="import" href="bower_components/ev-budget-planner-public/ev-budget-planner.html">
    <script src="bower_components/webcomponentsjs/webcomponents-loader.js" defer></script>
  </head>
  <body>
    <div>
      <h3> Component Demo</h3>
      <ev-budget-planner></ev-budget-planner>
    </div>
  </body>
</html>
```

## Proxy Service Endpoint
---

If you wish to conceal the authentication token from client side inspection, you can provide your own proxy service endpoint
to incercept, create and forward the API Facade request. You **cannot** define both the authentication token and proxy service endpoint.

```html
<ev-budget-planner proxy="http://www.myproxyendpoint.co.uk"></ev-budget-planner>
```

This particular widget makes 2 API calls:

* HTTP GET request to obtain the budget items and typical high/average/low net monthly spend for each one.
* HTTP POST request to calculate the net amounts of the each budget item that has been selected based on their group (essentials/desirables/luxuries).

So in order for a proxy service to handle these requests, you must provide `/getBudgetParams` and `/calculateTarget` as URI paths to capture and foward the appropriate requests to EValue's API portal.

## Custom CSS Properties
---

Below are some use CSS Variables which you can override to further customise the styling of
the budget planner widget.

Custom property | Description | Default
----------------|-------------|----------
`--ev-category-background-color` | The background color of each category | `#F7F5F2`
`--ev-header-background-color` | The background color of the table headers for each category | `#646464`
`--ev-budget-planner-essentials-color` | The main color which acts as the theme for the essentials category | `--ev-spearmint-500`
`--ev-budget-planner-desirables-color` | The main color which acts as the theme for the desirables category | `--ev-mango-400`
`--ev-budget-planner-luxuries-color` | The main color which acts as the theme for the luxuries category | `--ev-tomato-400`
`--ev-monthly-total-font-size` | The font size of the monthly total of each category | `16px`

```html
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes">
    <title>Personalised video demo</title>
    <script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
    <script src="bower_components/ev-budget-planner-public/ev-budget-planner.html"></script>
    <!-- custom-style element invokes the custom properties polyfill -->
    <script src="bower_components/polymer/lib/elements/custom-style.html"></script>
  </head>

 <!-- ensure that custom props are polyfilled on browsers that don't support them -->
  <custom-style>
    <style>
      ev-budget-planner {
        --ev-monthly-total-font-size: 20px;
        --ev-category-background-color: blue;
        --ev-header-background-color: red;
      }
    </style>
    ...
  </custom-style>
```

## Polyfills
---

As HTML Web Components use a set of new standards, these are not natively supported by older browsers and require a set of polyfills so that all features work as expected. The polyfills Javascript library contains a ‘loader’ script which will check the user’s browser to see which of the four main specifications that web components are based on are supported. It will then load the polyfills required for the component to display and function correctly.

Here are a list of polyfills you might want to include:

**webcomponents-lite.js** includes all of the polyfills necessary to run on any of the supported browsers. Because all browsers receive all polyfills, there is an extra overhead when using this.

**webcomponents-loader.js** performs client-side feature-detection and loads just the required polyfills. This requires an extra round-trip to the server but saves bandwidth for browsers that support one or more features.

**custom-elements-es5-adapter.js** essentially wraps ES5 compiled Custom Elements to work across browsers. This polyfill must load **before** defining a ES5 Custom Element. This adapter will automatically wrap ES5. This adapter must **NOT** be compiled.

```html
<script src="bower_components/webcomponentsjs/custom-elements-es5-adapter.js"></script>
<script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
```

Click [here](https://github.com/webcomponents/webcomponentsjs) to learn more about **webcomponentsloader.js** and the other polyfills listed above.