const puppeteer = require('puppeteer');
const { startServer } = require('polyserve');
const path = require('path');
const fs = require('fs');
const baselineDir = `${process.cwd()}/test/integration/screenshots-baseline`;
describe('regenerate screenshots for testing purposes', function () {
  let polyserve, browser, page;

  before(async function () {
    polyserve = await startServer({ port: 4444, root: path.join(__dirname, '../../..'), moduleResolution: 'node' });

    // Create the test directory if needed.
    if (!fs.existsSync(baselineDir)) {
      fs.mkdirSync(baselineDir);
    }
  });

  after((done) => polyserve.close(done));

  beforeEach(async function () {
    browser = await puppeteer.launch();
    page = await browser.newPage();
    page.setViewport(({width: 1902, height: 1366}));
  });

  afterEach(() => browser.close());

  it('did it', async function () {
    return generateBaselineScreenshots(page);
  });
});

function selectBudgetItems(page) {
  page.evaluate(() => {
    function searchShadowElementById(element, id) {
      let result;
      if (element.id === id) {
        return element;
      } else {
        let shadowChildren = element.shadowRoot !== null ? Array.from(element.shadowRoot.children): []
        let children = Array.from(element.children);
        let allChildren = children.concat(shadowChildren);
        if (allChildren.length !== 0) {
          for (let i = 0; i < allChildren.length; i++) {
            let child = allChildren[i];
            result = searchShadowElementById(child, id);
            if (result !== undefined) {
              break;
            }
          }
        }
      }
      return result;
    }
    const widget = document.querySelector('ev-budget-planner');
    searchShadowElementById(widget, 'essentials_average_all').click();
    searchShadowElementById(widget, 'desirables_low_all').click();
    searchShadowElementById(widget, 'luxuries_high_all').click();
    searchShadowElementById(widget, 'but_calculate').click();
  });
}


async function generateBaselineScreenshots(page) {
  await page.goto('http://127.0.0.1:4444/components/ev-budget-planner/demo');
  await page.waitFor(1500);
  selectBudgetItems(page)
  await page.waitFor(3000);
  await page.screenshot({ path: `${baselineDir}/index.png`, fullPage: true });
}
