const puppeteer = require('puppeteer');
const expect = require('chai').expect;
const { startServer } = require('polyserve');
const path = require('path');
const fs = require('fs');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const currentDir = `${process.cwd()}/test/integration/screenshots-current`;
const baselineDir = `${process.cwd()}/test/integration/screenshots-baseline`;

describe('👀 page screenshots are correct', function () {
  let polyserve, browser, page;

  before(async function () {
    polyserve = await startServer({ port: 4444, root: path.join(__dirname, '../..'), moduleResolution: 'node' });

    // Create the test directory if needed.
    if (!fs.existsSync(currentDir)) {
      fs.mkdirSync(currentDir);
    }
  });

  after((done) => polyserve.close(done));

  beforeEach(async function () {
    browser = await puppeteer.launch();
    page = await browser.newPage();
    page.setViewport(({width: 1902, height: 1366}));
  });

  afterEach(() => browser.close());

  describe('screen', function () {
    it('demo', async function () {
      return takeAndCompareScreenshot(page);
    });
  });
});

function selectBudgetItems(page) {
  page.evaluate(() => {
    function searchShadowElementById(element, id) {
      let result;
      if (element.id === id) {
        return element;
      } else {
        let shadowChildren = element.shadowRoot !== null ? Array.from(element.shadowRoot.children): []
        let children = Array.from(element.children);
        let allChildren = children.concat(shadowChildren);
        if (allChildren.length !== 0) {
          for (let i = 0; i < allChildren.length; i++) {
            let child = allChildren[i];
            result = searchShadowElementById(child, id);
            if (result !== undefined) {
              break;
            }
          }
        }
      }
      return result;
    }
    const widget = document.querySelector('ev-budget-planner');
    searchShadowElementById(widget, 'essentials_average_all').click();
    searchShadowElementById(widget, 'desirables_low_all').click();
    searchShadowElementById(widget, 'luxuries_high_all').click();
    searchShadowElementById(widget, 'but_calculate').click();
  });
}

async function takeAndCompareScreenshot(page) {
  // If you didn't specify a file, use the name of the route.
  await page.goto('http://127.0.0.1:4444/components/ev-budget-planner/demo');
  await page.waitFor(1500);
  selectBudgetItems(page);
  await page.waitFor(3000);
  await page.screenshot({path: `${currentDir}/index.png`, fullPage: true});
  return compareScreenshots();
}

function compareScreenshots() {
  return new Promise((resolve, reject) => {
    // Note: for debugging, you can dump the screenshotted img as base64.
    // fs.createReadStream(`${currentDir}/index.png`, { encoding: 'base64' })
    //   .on('data', function (data) {
    //     console.log('got data', data)
    //   })
    //   .on('end', function () {
    //     console.log('\n\n')
    //   });
    const img1 = fs.createReadStream(`${currentDir}/index.png`).pipe(new PNG()).on('parsed', doneReading);
    const img2 = fs.createReadStream(`${baselineDir}/index.png`).pipe(new PNG()).on('parsed', doneReading);

    let filesRead = 0;
    function doneReading() {
      // Wait until both files are read.
      if (++filesRead < 2) return;

      // The files should be the same size.
      expect(img1.width, 'image widths are the same').equal(img2.width);
      expect(img1.height, 'image heights are the same').equal(img2.height);

      // Do the visual diff.
      const diff = new PNG({ width: img1.width, height: img1.height });

      // Skip the bottom/rightmost row of pixels, since it seems to be
      // noise on some machines :/
      const width = img1.width - 1;
      const height = img1.height - 1;

      const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data,
        width, height, { threshold: 0.2 });
      const percentDiff = numDiffPixels / (width * height) * 100;

      const stats = fs.statSync(`${currentDir}/index.png`);
      const fileSizeInBytes = stats.size;
      console.log(`📸 index.png => ${fileSizeInBytes} bytes, ${percentDiff}% different`);

      //diff.pack().pipe(fs.createWriteStream(`${currentDir}/index-diff.png`));
      expect(numDiffPixels, 'number of different pixels').equal(0);
      resolve();
    }
  });
}
